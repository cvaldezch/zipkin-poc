const path = require('path');

  module.exports = {
    entry: [
      './src/zipkinBuilder.js',
      './src/client.js'],
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, ''),
      libraryTarget: 'var',
      library: 'call'
    },
    node: { fs: 'empty', net: 'empty', tls: 'empty', __filename: true, __dirname: true },
    target: "web",
    mode: "development",
    optimization: {
      // We no not want to minimize our code.
      minimize: false
    }
  };