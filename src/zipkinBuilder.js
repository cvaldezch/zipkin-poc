import zipkin from "zipkin";
import { HttpLogger } from "zipkin-transport-http";

const tracer = new zipkin.Tracer({
  ctxImpl: new zipkin.ExplicitContext(),
  recorder: new zipkin.BatchRecorder({
    logger: new HttpLogger({
      endpoint: 'http://192.168.99.100:9411/api/v2/spans',
      jsonEncoder: zipkin.jsonEncoder.JSON_V2,
      fetch
    })
  }),
  sampler: new zipkin.sampler.CountingSampler(1),
  localServiceName: "salesforce"
});

export { tracer, zipkin }

// # https://medium.com/@dschmidt1992/performance-monitoring-for-the-frontend-using-zipkin-bf3aa4a715e5