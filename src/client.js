import { tracer, zipkin } from "./zipkinBuilder"

window.onload = function() {
    const title = "MAIN IN READY"
    console.log(title)
    document.querySelector(".main_title").innerHTML = title
    console.log(tracer)
}


export const clickTest = () => {
  console.log("Yeah!!! entry client.")
}

export const clickCallback = () => {
  tracer.scoped(() => {
      const id = tracer.createRootId();
      console.log(id);
      tracer.setId(id);
      tracer.recordServiceName("FronEnd");
      tracer.recordAnnotation(new zipkin.Annotation.ClientSend());
      tracer.recordAnnotation(new zipkin.Annotation.Rpc("Span Country"));
    
      fetch("https://restcountries.eu/rest/v2/all")
      .then(res => res.json())
      .then(result => {
        console.log(result);
        tracer.scoped(() => {
          tracer.setId(id);
          let rshow = document.querySelector("#result");
          rshow.innerHTML = ""
          rshow.innerText = JSON.stringify(result);
          tracer.recordBinary("result", JSON.stringify(result));
          tracer.recordAnnotation(new zipkin.Annotation.ClientRecv());
        });
      });
    
    });
}

export const currencyCallback = () => {
  tracer.scoped(() => {
    let id = tracer.createRootId()
    tracer.setId(id)
    tracer.recordServiceName("restcountries.eu")
    tracer.recordAnnotation(new zipkin.Annotation.ClientSend())
    tracer.recordAnnotation(new zipkin.Annotation.Rpc("Span Currency"))

    fetch("https://restcountries.eu/rest/v2/currency/cop")
      .then(res => res.json())
      .then(result => {
        console.log(result);
        tracer.scoped(() => {
          tracer.setId(id);
          tracer.recordBinary("result", JSON.stringify(result));
          tracer.recordAnnotation(new zipkin.Annotation.ClientRecv());
          let rshow = document.querySelector("#result");
          rshow.innerHTML = ""
          rshow.innerText = JSON.stringify(result);
        });
      });
  })
}

export default { tracer }